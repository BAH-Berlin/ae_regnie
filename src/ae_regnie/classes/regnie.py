# -*- coding: utf-8 -*-
"""
Created on Fri Jan 22 20:16:29 2021

@author: ruben.mueller
"""

import os
import zipfile
import dbf
import datetime
import json
import requests
import shutil
import tarfile, gzip
import numpy as np
from collections.abc import Iterable

import base_bah as buk

DEBUG = False

try: 
    from PyQt5 import QtCore
    Qsignal = QtCore.pyqtSignal
    QObject = QtCore.QObject
except:
    class Qsignal:
        def __init__(self, a):
            pass
        def emit(self, a):
            pass
    class QObject:
        def __init__(self, a):
            pass


def https(year, filename, timeout=300, headers=None):
    """Download a link to a file. FAILS if SSL certificate is bogus.

     Parameters
     ----------
     urlLink : str
         link to the server folder
     filename : str
         path and filename for download file
    """
    headers = {} if not headers else headers
    urlLink = "https://opendata.dwd.de/climate_environment/CDC/grids_germany/daily/regnie/ra{}m.tar".format(year)
    r = requests.get(urlLink, timeout=timeout, headers=headers,
        verify=False, stream=True)
    if r.status_code == 200:
        r.raw.decode_content = True
        with open(filename, 'wb') as f:
            try:
                shutil.copyfileobj(r.raw, f)
            except Exception as e:
                raise e
        extrdir = os.path.join(os.path.dirname(filename), 'ra' + str(year) + 'm')
        if not os.path.isdir(extrdir):
            os.mkdir(extrdir)

        tar = tarfile.open(filename)
        tar.extractall(path=extrdir)
        tar.close()
        return 1
    else:
        return 0


def get_prec_doy(bfolder, r_date):
    """Read a compressed regnie file for a single day and return the content.

    Parameters
    ----------
    bfolder : str
        download folder
    r_date : datetime.datetime
        the date to read for

    Returns
    -------
    r_day_col : np.array
        the content reshaped into a column vector"""
    r_day_col = None
    
    def readx(f):
        r_day = np.genfromtxt(f, delimiter=4, max_rows=971)
        r_day_col = np.reshape(r_day, (r_day.size))
        return r_day_col
    
    if not r_date is None:
        x = str(datetime.datetime.strftime(r_date, 'ra%y%m%d.gz'))
        y = str(datetime.datetime.strftime(r_date, 'ra%Ym'))
        with gzip.open(os.path.join(bfolder, y, x), 'rt') as f:
            r_day = np.genfromtxt(f, delimiter=4, max_rows=971)
            r_day_col = np.reshape(r_day, (r_day.size)) 
    else:
        archive = zipfile.ZipFile(os.path.join(bfolder, "RA701231.ZIP"), 'r')
        with archive.open("RA701231") as f:
            r_day = np.genfromtxt(f, delimiter=4, max_rows=971)
            r_day_col = np.reshape(r_day, (r_day.size)) 
    return r_day_col


def generate_mapping_base_files(RAS6190_full, outputfolder=None):
    """Generate relation files points--column in pklz form and save them.

    Parameters
    ----------
    RAS6190_full : str
        path and name of the RAS6190_full file which holds the coordinates
    outputfolder: str
        folder to write the output files into
    """
    if not outputfolder:
        outfile_full = os.path.join(os.path.dirname(RAS6190_full), 'full_IDs.pklz')
        #outfile_red = os.path.join(os.path.dirname(RAS6190_full), 'reduced_IDs.pklz')
    else:
        outfile_full = os.path.join(outputfolder, 'full_IDs.pklz')
        #outfile_red = os.path.join(outputfolder, 'reduced_IDs.pklz')

    r_day = np.genfromtxt(RAS6190_full, delimiter=',', skip_header=1)
    r_day[:, 2] -= 1
    r_day = np.concatenate((r_day, np.zeros((np.shape(r_day)[0], 1))), axis=1)
    
    c = 0
    for i, x in enumerate(r_day):
        if  x[3] != -1:
            r_day[i] = c
            c += 1
    buk.pkl.save_obj_compressed(outfile_full, r_day)


###############################################################################
###############################################################################


class Regnie_data(QObject):
    
    notifyProgress = Qsignal(float)

    def __init__(self):
        super().__init__()
        self.__executable = True
        self.__id = "regnie_creator"
        self.__class = "regnie_runner"
        self.__target_dlfolder = None
        self.__target_outfolder = None
        self.__target_pklzfolder = None
        self.server = "opendata.dwd.de"
        self.dwd_folder = 'climate_environment/CDC/grids_germany/daily/regnie/'
        self.user = ""
        self.pswrd = ""
        self.isslave = False
        self.list_id = []
        self.list_id_in = []
        self.current_time = datetime.datetime.utcnow()
        rrs = os.path.join(os.path.dirname(__file__), os.pardir)
        rrs = os.path.join(os.path.abspath(rrs), "pklz")
        rrs1 = os.path.join(rrs, "full_IDs.pklz")
        self.full_regnie_set = buk.pkl.load_obj_compressed(rrs1)
        self.__slot = datetime.datetime(1970, 1, 1)

    @property
    def lf_slot(self):
        '''The run type differentiates between user defined runs and continuous model runs.'''
        return self.__slot

    @lf_slot.setter
    def lf_slot(self, time_slot):
        '''The run type is either a user defined run (USERR) or a continuous model runs (CMR).'''
        if isinstance(time_slot, datetime.datetime):
            self.__slot = time_slot
        if isinstance(time_slot, list):
            self.__slot = datetime.datetime(*time_slot)
        
    @property
    def id(self):
        """Return if executable"""
        return self.__id
        
    @property
    def executable(self):
        """Return if executable"""
        return self.__executable
    
    @property
    def iclass(self):
        """Return the class identificator."""
        return self.__class

    @property
    def current_date(self):
        return self.__target_dlfolder

    @current_date.setter
    def current_date(self, current_date, dformat="%d.%m.%Y"):
        """Folder for the downloaded RADOLAN data."""
        if not os.path.isinstance(current_date, datetime.datetime):
            try:
                current_date = datetime.datetime.strptime(current_date, dformat)
            except:
                raise ValueError("Could not recognize the date with format {}".format(dformat))
        else:
            self.__current_date = current_date


    @property
    def download_folder(self):
        return self.__target_dlfolder

    @download_folder.setter
    def download_folder(self, folder):
        """Folder for the downloaded RADOLAN data."""
        if not os.path.isdir(folder):
            raise FileNotFoundError("Path %s not available!" % folder)
        else:
            self.__target_dlfolder = folder


    @property
    def output_folder(self):
        return self.__target_outfolder

    @output_folder.setter
    def output_folder(self, folder):
        """Folder for the downloaded RADOLAN data."""
        if not os.path.isdir(folder):
            raise FileNotFoundError("Path %s not available!" % folder)
        else:
            self.__target_outfolder = folder


    @property
    def pklz_folder(self):
        return self.__target_pklzfolder

    @pklz_folder.setter
    def pklz_folder(self, folder):
        """Folder for the downloaded RADOLAN data."""
        if not os.path.isdir(folder):
            raise FileNotFoundError("Path %s not available!" % folder)
        else:
            self.__target_pklzfolder = folder


    def clear_download_folder(self, test=False):
        """Delete all downloaded gz-files in the download folder."""
        if self.__target_dlfolder is None:
            raise ValueError("Download folder undefined. Please set download_folder property first.")
        flist = [f for f in os.listdir(self.__target_dlfolder) if os.path.isfile(f)]
        if test:
            print("Found {} files in {} to delete.".format(len(flist),
                 self.__target_dlfolder))
        else:
            for file in flist:
                    os.remove(file)
                    
                    
    def generate_missing_date70(self):
        """download the RA701231.ZIP which is needed to correct for the
        switch in measurement date mapping at with the start of 1971."""
        mfile = "RA701231.ZIP"
        url = self.server + "/" + self.dwd_folder + "/" + mfile
        if not url[:8] == "http://":
            url = "http://" + url
        try:
            zipfile = os.path.join(self.__target_dlfolder, mfile)
            if not os.path.isfile(zipfile):
                buk.download.https(url, filename=zipfile)
        except Exception as e:
            raise ValueError("download failed %s" % e)
        regnie_vec = get_prec_doy(self.__target_dlfolder, None)
        outfile = os.path.join(self.__target_pklzfolder, "mfile_1970.pklz")
        X = regnie_vec[self.reduced_set].T
        buk.pkl.save_obj_compressed(outfile, X.astype(int))


    def generate_regnie_pklz(self, end_year=None, start_year=None, redownload=False):
        """Generate an pklz file for each day of year.

        Parameters
        ----------
        config : dict
            the dict has to following keys:
            start_year : int
                start year to create pklz
            reduced_regnie_set : np.array
                array with where each row maps to the row in the complete regnie set
            bolder : str
                download folder, creates a DL an PKLZ subfolders if not present
            end_year : int, optional
                sets the endyear to create pklz
        redownload : bool
            download regnie files again and overwrite"""

        if start_year is None:
            start_year = datetime.datetime.utcnow().year
        elif isinstance(start_year, datetime.datetime):
            start_year = start_year.year
            
        if end_year is None:
            end_year = start_year + 1
        elif isinstance(end_year, datetime.datetime):
            end_year = end_year.year + 1 

        for year in range(start_year, end_year):

            if not os.path.isdir(os.path.join(self.__target_pklzfolder, str(year))):
                os.mkdir(os.path.join(self.__target_pklzfolder, str(year)))

            dlfile = os.path.join(self.__target_dlfolder, "ra{}m.tar".format(year))
            if not os.path.isfile(dlfile) or redownload:
                https(year, dlfile)

            ### loop over the days of the year and insert data
            startdate = datetime.datetime(year, 1, 1)
            for i in range(0, 367):
                r_date = startdate + datetime.timedelta(days=i)
                if r_date.year == year+1:
                    break
                outfile = os.path.join(self.__target_pklzfolder, str(year),
                    datetime.datetime.strftime(r_date, 'ra%Y%m%d.pklz'))
                if not os.path.isfile(outfile):
                    try:
                        regnie_vec = get_prec_doy(self.__target_dlfolder, r_date)
                    except FileNotFoundError:
                        print("No data available for {}".format(r_date))
                        break
                    X = regnie_vec[self.reduced_set].T
                    buk.pkl.save_obj_compressed(outfile, X.astype(int))


    def ids_read_shp(self, cfile=None, key=None, selection=None):
        """Read a shape file for RADOLAN-IDs.

        Parameters
        ----------
        cfile : str
            A shape or dbf file
        key : str
            the key oder name of the column with the RADOLAN-IDs ... IDs start with 1!
        selecion : dict or None
            for mixed data bases, dictionary selection provides two keys
            {column: 'the column name with the types',
             type: 'the identifier (e.g. m)'}"""
        if not os.path.isfile(cfile):
            raise FileNotFoundError("Shape file %s not found." % cfile)

        dbffile = cfile[:-3] + "dbf"
        if not os.path.isfile(dbffile):
            raise FileNotFoundError("Shape file %s has no DBF file!" % dbffile)

        try:
            table = dbf.Table(dbffile)
            table.open()
            contents = table.field_names
            if not key.lower() in contents:
                raise KeyError("key not found! We have: {}".format(contents))

            self.list_id_in = []

            if selection is None:
                for k in table:
                    self.list_ids.append(k[contents.index(key.lower().strip())])
            else:
                if not isinstance(selection, dict):
                    raise ValueError("Failure reading the selction section in shape_file.")
                if not "column" in selection or not "type" in selection:
                    raise ValueError("Failure in the selction section in shape_file. Keys column and type must be provided")

                if not key.lower() in contents:
                    raise KeyError("key not found! We have: {}".format(selection["column"]))
                if not selection["column"].lower() in contents:
                    raise KeyError("key not found! We have: {}".format(contents))

                tmp_list = []
                m_list =[]
                selection["type"] = "" if selection["type"] is None else selection["type"]
                for k in table:
                    tmp_list.append(k[contents.index(key.lower().strip())])
                    m_list.append(k[contents.index(selection["column"].lower().strip())])
                self.list_id_in = [x[1] - 1 for x in enumerate(tmp_list) if m_list[x[0]].strip() == selection["type"]]

        except Exception as e:
            raise(e)
        finally:
            table.close()
            #print(self.list_ids)


    def generate_mapping_pklz(self):
        """Generate an mapping file"""
        if not self.list_id_in:
            raise ValueError("Please provide the REGNIE grid IDs for your project first")
            
        self.reduced_set = self.full_regnie_set[np.where(self.full_regnie_set[:,3]!=-1), 2][0,:].astype(int).T# this is the reduced set, that we keep on disc
        
        mf = np.zeros((len(self.list_id_in), 2)).astype(int)
        mf[:, 0] = self.list_id_in                      # the REGNIE-IDs of the project ("official IDs")
        mf[:, 1] = self.full_regnie_set[mf[:, 0], -1]   # the IDs in the reducted set (what is on disc)

        # ## check for -1 in the list of ids
        if len(np.argwhere(self.full_regnie_set[mf[:, 0], 3] == -1)):
             print("check your list of IDs. Some may to be outside the REGNIE area with measured data.")

        self.list_id = mf



    def ids_by_list(self, list_id, id_start_with_one=True):
        """Provide a list with Radolan-IDs.

        Parameters
        ----------
        list_id : Iterable
            list with ID full set
        id_start_with_one : bool
            True, if both the regnie ID and the reduced regnie IDs start with one"""
        if not isinstance(list_id, Iterable):
            raise ValueError("Provided list_id is not iterable.")
        self.list_id_in = list(list_id)

        if id_start_with_one:
            self.list_id_in = [x-1 for x in list_id]


    def ids_by_listfile(self, listfile, id_start_with_one=True):
        """Read a list from an ASCII file
        A CSV file with regnie ID and the reduced regnie IDs
        id_start_with_one : bool
            True, if both the regnie ID and the reduced regnie IDs start with one"""
        self.list_id_in = list(np.fromfile(listfile, dtype=float))
        print("read {} IDs".format(len(self.list_id)))
        
        if id_start_with_one:
            self.list_id_in = [x-1 for x in self.list_id]


    def read_config(self, configfile):
        """Read settings from a json file."""
        with open(configfile, "r") as fid:
            config_r = json.load(fid)

        if not "download_folder" in config_r:
            raise ValueError("Keyword download_folder not found.")
        else:
            self.download_folder = config_r["download_folder"]

        if not "pklz_folder" in config_r:
            raise ValueError("Keyword pklz_folder not found.")
        else:
            self.pklz_folder = config_r["pklz_folder"]

        if not "output_folder" in config_r:
            raise ValueError("Keyword output_folder not found.")
        else:
            self.output_folder = config_r["output_folder"]
            
        try:
            config_r["shape_file"]["file"]
            config_r["shape_file"]["column"]
            config_r["shape_file"]["selection"]
            self.ids_read_shp(config_r["shape_file"]["file"],
                config_r["shape_file"]["column"],
                config_r["shape_file"]["selection"])
        except:
            print("did not read the shape file")

        try:
            config_r["list_file"]["file"]
            self.ids_read_list(config_r["list_file"]["file"])
        except:
            print("did not read the list file")


    def write_gis_day_all(self, day):
        """write an csv file for a specific day which can be imported in GIS.
        
        Parameters
        ----------
        day : datetime.datetime
            the day of interest"""
        dayf = datetime.datetime.strftime(day, "%Y%m%d")
        regnie_file = os.path.join(self.__target_outfolder, '{}.txt'.format(dayf))
       
        fregnie = os.path.join(self.__target_pklzfolder,
                    str(day.year), datetime.datetime.strftime(day, 'ra%Y%m%d.pklz'))
        cregnie = buk.pkl.load_obj_compressed(fregnie)
        c = 0
        with open(regnie_file, "w") as fid:
            fid.write("Y\tX\tID\tP\n")
            for i, x in enumerate(self.full_regnie_set):
                if x[3] != -1:
                    fid.write("{}\t{}\t{}\t{}\n".format(x[0], x[1], int(x[2]), cregnie[c]))
                    c += 1


    def write_gis_day_sel(self, day):
        """write an csv file for a specific day which can be imported in GIS.
        
        Parameters
        ----------
        day : datetime.datetime
            the day of interest"""
        dayf = datetime.datetime.strftime(day, "%Y%m%d")
        regnie_file = os.path.join(self.__target_outfolder, '{}.txt'.format(dayf))
        listID = self.list_id
        fregnie = os.path.join(self.__target_pklzfolder,
                    str(day.year), datetime.datetime.strftime(day, 'ra%Y%m%d.pklz'))
        cregnie = buk.pkl.load_obj_compressed(fregnie)
        with open(regnie_file, "w") as fid:
            fid.write("Y\tX\tID\tReducedID\tP\n")
            for i, x in enumerate(listID):
                fid.write("{}\t{}\t{}\t{}\t{}\n".format(self.full_regnie_set[x[0], 0], 
                    self.full_regnie_set[x[0], 1], int(self.full_regnie_set[x[0], 2]), 
                    x[1]+1, cregnie[x[1]]))


    def update_ts(self):
        """Run the download and writting. NieTro2"""
        self.generate_mapping_pklz()
        self.generate_regnie_pklz(redownload=True)
        self.generate_regnie_arcegmo(period=True)
        return True
    
    
    def selfcheck(self):
        """Check the setup."""
        if any([x is None for x in (self.reduced_regnie_set, self.current_time,
        self.idx_reduced_set, self.bfolder, self._model_run, self.__executable, self.conf)]):
            raise ValueError("{} was fully set up correctly yet".format(self))
    
    def __str__(self):
        return 'Regnie with id {}'.format(self.id)
    
    def __repr__(self):
        return 'Regnie(%r)' % (self.id)


    def generate_regnie_arcegmo(self, start_endtimes=False, period=False):
        """Create the REGNIE precipitation input file.

        Parameters
        ----------
        start_endtimes : list of datetime.datetime
            overwrite current_time, start with start_endtimes[0] and end with start_endtimes[1]"""

        regnie_file = os.path.join(self.__target_outfolder, 'regnie.N')
        
        self.generate_mapping_pklz()

        if not start_endtimes:
            startdate = datetime.datetime(self.current_time.year, 1, 1)
            enddate = self.conf.time
        else:
            startdate = start_endtimes[0]
            enddate = start_endtimes[1]
        if period:
            startdate = enddate - datetime.timedelta(days=self.period)
        period  = (enddate - startdate).days
        
        self.generate_regnie_pklz()

        list_IDr = self.list_id[:, 1]
        print("writing file...")
        with open(regnie_file, "w") as fid:
            fid.write("Termin\t" + "\t".join([str(x) \
                for x in self.list_id[:, 0] + 1]) + "\n")

            date71 = datetime.datetime(1971, 1, 1)                

            for dss in range(period):
                cdate = startdate + datetime.timedelta(days=dss)
                
                if cdate < date71:
                    ccdate = cdate - datetime.timedelta(days=1)
                else:
                    ccdate = cdate

                fregnie = os.path.join(self.__target_pklzfolder,
                    str(cdate.year), datetime.datetime.strftime(cdate, 'ra%Y%m%d.pklz'))
                try:
                    cregnie = buk.pkl.load_obj_compressed(fregnie)[list_IDr]
                    fid.write(datetime.datetime.strftime(ccdate, "%d.%m.%Y\t"))
                    fid.write("\t".join([str(x) for x in cregnie / 10]) + "\n")
                    
                    if ccdate == datetime.datetime(1970, 12, 30):
                        fregnie = os.path.join(self.__target_pklzfolder,'mfile_1970.pklz')
                        if not os.path.isfile(fregnie):
                            self.generate_missing_date70()
                        cregnie = buk.pkl.load_obj_compressed(fregnie)[list_IDr]
                        ccdate = cdate
                        fid.write(datetime.datetime.strftime(ccdate, "%d.%m.%Y\t"))
                        fid.write("\t".join([str(x) for x in cregnie / 10]) + "\n")
                    if not dss % 10:
                        status = float(dss)/float(period-1)
                        buk.output.update_progress(status)
                        if self.isslave: self.notifyProgress.emit(status)
            
                except:
                    print("ending prematurely with doy {}, file {} not available.".format(dss, fregnie))
                    break
            buk.output.update_progress(100.)
            if self.isslave: self.notifyProgress.emit(1)


def regnie_create():
    """Class factory for Radolan_data"""
    return Regnie_data()


# generate_mapping_base_files("E:\\regnie\\download_Regnie2GIS\\Regnie2GIS\\ra210208_full.csv")

if 0:
    reg = regnie_create()
    reg.read_config("e:/regnie/settings.json")
    # reg.list_id
    #reg.ids_read_shp("E:\\Spree\\Modell\\GIS\\regnie_dwd_1km_spree.shp", 
    #    key="regnie1km", selection={"column":"typ", "type": "m"})
    # reg.generate_mapping_pklz()
    # reg.generate_missing_date70()
    
    start = datetime.datetime(2010, 1, 1)
    end = datetime.datetime(2011, 1, 1)
    # reg.generate_regnie_pklz(end_year=end, start_year=start)
    reg.generate_regnie_arcegmo(start_endtimes=[start, end])
    # reg.write_gis_day_sel(datetime.datetime(2021, 2, 8))