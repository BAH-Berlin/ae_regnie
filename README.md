# ae_regnie
Download daily [REGNIE](https://www.dwd.de/DE/leistungen/regnie/regnie.html) interpolated precipitation data from the [DWD serveropen](https://opendata.dwd.de/climate_environment/CDC/grids_germany/daily/regnie/), store it as compressed numpy-arrays (reduced to grid points with data) and create precipitation time series for [ArcEGMO](https://www.arcegmo.de).

![ae_regnie figure](doc/static/ae_regnie.png)

## Usage

Create an regnie object and set the configuration. REGNIE data from the DWD server is stored in the folder __download_folder__. The Downloads are only required temporaly and can be deleted at the end.

For storage and speed-up the data will be stored as compressed numpy-arrays in the folder __pklz_folder__. The following code section is required for a) to d).

```
import ae_regnie
reg = regnie_create()
reg.download_folder = "./folder/download"
reg.pklz_folder = "./folder/pklz"
```
### a) Download and store data
Download all data from the years 2013 and 2014 without creating precipitation input files for ArcEGMO

```
reg.generate_regnie_pklz(start_year=2013, end_year=2021)
```

From the [REGNIE description](https://opendata.dwd.de/climate_environment/CDC/grids_germany/daily/regnie/DESCRIPTION_gridsgermany_daily_regnie_en.pdf):
"Since 01.01.1971 precipitation height is attributed to the day which constitutes thebulk of the measurement interval (i.e., the day before the precipitation height reading is taken). Before 1971, the date on which theprecipitation height reading was used as the time reference. To get a contineous time attribution, the grids related to times before 1971 have to be shifted one day back in time."

Therefore the missing day 31.12.1970 has to be created seperately at one time:

```
reg.generate_missing_date70()
```
### b) Set the REGNIE-GridIDs 
You have several options to provide the REGNIE-GridIDs of your project. Note that the REGNIE-GridIDs for the complete grid start in the upper-left corner with an ID of __1__. _One of the steps 1. to 3. is necessary if you have not already created an mapping file, otherwise use 4._:

#### Option 1: Shape file
You can provide an shape-file which lists all Grid-IDs of your project. An input time series will only be written for these IDs.

```
reg.ids_read_shp("./folder/regnie_project.shp", 
    key="regnie1km", selection={"column":"typ", "type": "m"})
```
Where __key__ is the column name with the REGNIE-GridIDs and the dictionary for __selection__ gives the __column__ which provides the feature __types__. The __selection__ argument is optional and is only needed if the shape file holds REGNIE-Grid-Points _and_ other features like DWD climate stations. In this case only features are read, that match the given identifier __type__, here _m_.

#### Option 2: Provide a list
Specifiy a list with the REGNIE-GridIDs. By default it is assumed that the IDs of the complete REGNIE grid start with __1__. If this is not the case (complete REGNIE grid start with __0__), set the argument optional __id_start_with_one__ to _False_.

```
reg.ids_by_list([23433, 23456, 565654], id_start_with_one=True)
```

#### Option 3: Provide a list file
Specifiy a file with the REGNIE-GridIDs, each in a new line. By default it is assumed that the IDs of the complete REGNIE grid start with __1__. If this is not the case (complete REGNIE grid start with __0__), set the argument optional __id_start_with_one__ to _False_.

```
reg.ids_by_list("./folder/regnie_IDs.txt", id_start_with_one=True)
```

### c) Creating a project mapping file
A project mapping file holds the REGNIE-GridIDs for your project and the Information how the read the precipitation from the stored regnie pklz files. After you set the REGNIE-GridIDs in step b) call

```
reg.generate_mapping_pklz()
```

which will create the file in the __output_folder__. You must create this file to produce input files in step d) once for a project.

### d) Create precipitation input time series files
Typically only a subset of the area covered by RADOLAN is needed for a project. To limit the precipitation file to the area, the subset of grid-IDs must be provided. Note that the numbering of the RADOLAN-IDs begins at the lower left corner of the grid with _zero_ and ends at the upper right corner.</br>
To create the precipitation file, the data is downloaded on the fly, if not already done.

```
startdate = datetime.datetime(2013, 1, 1, 1, 0)
enddate = datetime.datetime(2014, 12, 31, 23, 55)
reg.read_mapping_pklz("./folder/regnie_project.pklz")
reg.generate_regnie_arcegmo(startdate, enddate)
```

### e) Create a file for GIS 
You can import this file in GIS and create a point layer with the precipitation for one day.

```
reg.write_gis_day_all()   # for Germany
reg.write_gis_day_sel()   # for the selected area
```

### d) Clear the download folder
Attention, this step deletes all files in the folder download_folder

```
import datetime
reg.clear_download_folder(datetime.datetime(2021, 2, 8))
```

### e) Working with a configuration file

#### The configuration file
A configuration file is handy, if you build precipitation time series regularly.
The configuration is written in json style, with a structure like this:

```
{
    "download_folder": "./folder/download",
    "pklz_folder": "./folder/pklz",
    "output_folder": "./folder",
	"mapping_file": "./folder/mapping_idx_full_to_reduced.pklz",
    "shape_file": {
        "file": "./folder/radolan_raster_p.shp",
        "column": "REGNIE1KM",
        "selection": {
            "column": "typ",
            "type": "m"
        }
    },
    "list_file": {
        "file": null
    }
}
```

The names in the json file are:

 * __download_folder__, _required_: the tar-files are downloaded into this folder
 * __pklz_folder__, _required_: the compressed numpy archives for the REGNIE files are saved into this folder.
 * __output_folder__, _required_: the precipitation time series is created in this folder.
 * __mapping_file__, _required_: pklz file with REGNIE-GridIDs for your project and the Information how the read the precipitation from the stored regnie pklz files.
 * __shape_file__, _required_: provides configuration to read the REGNIE -IDs from an ArcGIS shape file (from the respective dbf file). Not needed, if a __mapping_file__ for the project is given.
      * __file__, _required_: path and file name of the shape file. Set to __null__ if no shape file is needed.
	  * __column__, _required_: the field name with the REGNIE-IDs. Has no effect if file is null.
	  * __selection__, _required_: If the shape holds more then just REGNIE grid entries, you can specify an identifier in another field.
	     * __column__, _required_: the field name with the identifier. Set to __null__ if no identifier is needed.
		 * __type__, _required_: the identifier as a REGNIE entry. All other entries in the shape file which do not match will be ignored.
 * __list_file__, _required_: read the REGNIE -IDs from an ascii-file. Each ID in a new line. Not needed, if a __mapping_file__ for the project is given.
      * __file__, _required_: path and file name of the ascii file. Set to __null__ if no shape file 


#### Workflow example with configuration file
Create a precipitation time series from 01.Jan.2019 to 12.Jan.2021 using the settings.json and clean-up the download folder by deleting all files in it afterwards.

```
import ae_regnie
startdate = datetime.datetime(2015, 1, 1)
enddate = datetime.datetime(2021, 1, 13)
reg = ae_regnie.create()
reg.read_config("./folder/settings.json")

#Use generate_mapping_pklz only when a project mapping file is not available yet. 
#If file is NOT available, provide a shape file or a list file in the configuration file 
#and run generate_mapping_pklz. 
#If available, just specify the file in the configuration file and skip.
reg.generate_mapping_pklz() 

reg.generate_regnie_pklz(end_year=end, start_year=start)
reg.clear_download_folder() # attention, deletes all folders in the folder download_folder
```

## Required Python packages

 * [base_bah](https://bitbucket.org/BAH_Berlin/base_bah)
 * numpy

## Author
Ruben Müller  
Büro für Angewandte Hydrologie, Berlin  
ruben.mueller@bah-berlin.de  
[webpage](www.bah-berlin.de)

![BAH](doc/static/bah-logo.png)

## License
Copyright (C) 2021 Ruben Müller, Büro für Angewandte Hydrologie, Berlin  

This program is free software; you can redistribute it and/or modify  
it under the terms of the GNU General Public License as published by  
the Free Software Foundation; either version 1, or (at your option)  
any later version.

This program is distributed in the hope that it will be useful,  
but WITHOUT ANY WARRANTY; without even the implied warranty of  
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
GNU General Public License for more details.  

You should have received a copy of the GNU General Public License  
along with this program; if not, write to the Free Software  
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA  02110-1301 USA.
