# Always prefer setuptools over distutils
from setuptools import setup, find_packages
import pathlib

here = pathlib.Path(__file__).parent.resolve()

long_description = (here / 'README.md').read_text(encoding='utf-8')

setup(
    name="ae_regnie",
    version="0.1",
    author="Ruben Müller, Büro für Angewandte Hydrologie",
    author_email="software@bah-berlin.de",
    description="Download REGNIE precipitation data, store it as gziped-pickled numpy arrays and create ArcEGMO input time series.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/BAH_Berlin/ae_regnie",
    packages=find_packages(where="src"), 
    package_dir={'': 'src'}, 
    license = "gpl",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU Lesser General Public License v3 (LGPLv3)",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.5, <4',
    setup_requires=["numpydoc"],
    package_data={'ae_regnie': ['pklz/*.pklz']},
    #package_data={'negar/data': ['data/*.dat']},
    #data_files=[('', ['pklz/full_IDs.pklz'])],
    install_requires=["wheel", "base_bah", "numpy", "dbf"],
    entry_points = {
        'create_object': [
            'regnie_obj = ae_regnie.main:create'
        ]
    }
)
